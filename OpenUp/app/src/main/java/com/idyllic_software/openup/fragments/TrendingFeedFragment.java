package com.idyllic_software.openup.fragments;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.adapters.TrendingListAdapter;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.idyllic_software.openup.view.TrendingItem;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hp1 on 21-01-2015.
 */
public class TrendingFeedFragment extends Fragment {

    private ArrayList<JSONObject> list = null;
    private ListView listView;
    private TrendingListAdapter listadapter;
    Boolean loadingMore;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.trending_feed,container,false);


        listView = (ListView) v.findViewById(R.id.trending_list);
        listadapter = new TrendingListAdapter(getActivity(), R.layout.activities_item, list,this);
        listView.setAdapter(listadapter);

        if (OpenUpServiceManager.getInstance().getFeedArray().size()==0)
            loadTrending();
        else
            listadapter.updateList(OpenUpServiceManager.getInstance().getTrendingArray());

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if((lastInScreen == totalItemCount) && !loadingMore)
                {
                    loadNextPage();
                }
                else
                {
                    loadingMore = false;
                }
            }
        });
        return v;
    }


    private void loadNextPage()
    {
        if (OpenUpServiceManager.getInstance().getCurrentPageForTrending()<OpenUpServiceManager.getInstance().getTotalPageForTrending())
        {
            OpenUpServiceManager.getInstance().setCurrentTrendingPage(OpenUpServiceManager.getInstance().getCurrentPageForTrending() + 1);
            loadingMore = true;
            loadTrending();
        }
    }

    private void loadTrending()
    {
        OpenUpServiceManager.getInstance().fetchTrending(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                listadapter.updateList(OpenUpServiceManager.getInstance().getTrendingArray());
                if (listadapter.getCount() > 0) {
//                    refreshUI(mPagerAdapter.getItem(currentPage));
//                    noPostText.setVisibility(View.GONE);
                } else {
//                    noPostText.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                if (listadapter.getCount() > 0) {
//                    refreshUI(mPagerAdapter.getItem(currentPage));
//                    noPostText.setVisibility(View.GONE);
                } else {
//                    noPostText.setVisibility(View.VISIBLE);
                }

            }

        });
    }

}