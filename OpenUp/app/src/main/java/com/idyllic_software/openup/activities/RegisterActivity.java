package com.idyllic_software.openup.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.idyllic_software.openup.utils.OpenUpConstants;
import com.idyllic_software.openup.utils.SharedPreferenceManager;
import com.idyllic_software.openup.utils.Utility;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by javalnanda on 21/04/15.
 */
public class RegisterActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private EditText emailEditText;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        String errorMessage;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                errorMessage= null;
            } else {
                errorMessage= extras.getString("error_message");
            }
        } else {
            errorMessage= (String) savedInstanceState.getSerializable("error_message");
        }

        if(errorMessage!=null)
        {
            Utility.showAlertDialog(RegisterActivity.this,errorMessage);
        }

        OpenUpServiceManager.getInstance().initialize(RegisterActivity.this);

        emailEditText = (EditText) findViewById(R.id.email_editText);
        emailEditText.setMaxWidth(emailEditText.getWidth());
        emailEditText.setMaxHeight(emailEditText.getHeight());

        registerButton = (Button) findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(emailEditText.getText().toString().length()>0)
                {
                    if (Utility.validEmail(emailEditText.getText().toString()))
                    {
                        OpenUpServiceManager.getInstance().registerUser(emailEditText.getText().toString(),new JsonHttpResponseHandler()
                        {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                super.onSuccess(statusCode, headers, responseString);
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                super.onSuccess(statusCode, headers, response);

                                // In case email id already registered previously
                                if (response.has("user_token"))
                                {
                                    String userToken = null;
                                    try {
                                        userToken = response.getString("user_token");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if(userToken!=null)
                                    {
                                        // Navigate to main screen
                                        SharedPreferenceManager.savePreferences(RegisterActivity.this, OpenUpConstants.USER_TOKEN_PREFERENCES,userToken);
                                        Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(i);
                                    }
                                }
                                else
                                {
                                    OpenUpServiceManager.getInstance().setEmailToRegister(emailEditText.getText().toString());
                                    Intent i = new Intent(RegisterActivity.this, VerifyActivity.class);
                                    startActivity(i);
                                }

                                finish();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                super.onFailure(statusCode, headers, responseString, throwable);
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                super.onFailure(statusCode, headers, throwable, errorResponse);
                            }

                        });


                    }
                    else
                    {
                        Utility.showAlertDialog(RegisterActivity.this,getResources().getString(R.string.enter_valid_email));
                    }
                }
                else
                {
                    Utility.showAlertDialog(RegisterActivity.this,getResources().getString(R.string.enter_email));
                }

            }
        });
    }
}