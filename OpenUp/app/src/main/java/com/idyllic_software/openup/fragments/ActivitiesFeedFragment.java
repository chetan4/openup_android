package com.idyllic_software.openup.fragments;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.adapters.ActivitiesListAdapter;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.idyllic_software.openup.utils.Utility;
import com.idyllic_software.openup.view.FeedDetailItem;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hp1 on 21-01-2015.
 */
public class ActivitiesFeedFragment extends Fragment {

    private ArrayList<JSONObject> feedArrayList = null;
    private ListView listView;
    private ActivitiesListAdapter listadapter;

    private EditText composeEditText;
    private EditText commentEditText;

    private TextView textCounterText;
    private TextView commentTextCounter;

    ImageButton postButton;
    ImageButton postCommentButton;
    ImageButton notifyAdminButton;

    RelativeLayout postCommentLayout;
    RelativeLayout shareStatusLayout;
    Boolean loadingMore;

    JSONObject parentFeedData;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.activities_feed,container,false);

        View headerView = View.inflate(getActivity(), R.layout.share_status, null);


        listView = (ListView) v.findViewById(R.id.activities_list);
        listadapter = new ActivitiesListAdapter(getActivity(), R.layout.activities_item, feedArrayList,this);
        listView.addHeaderView(headerView);
        listView.setAdapter(listadapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDetailView(feedArrayList.get(position-1));
            }
        });


        postButton = (ImageButton) v.findViewById(R.id.postButton);
        composeEditText = (EditText) v.findViewById(R.id.post_edit_text);
        textCounterText = (TextView) v.findViewById(R.id.text_counter);

        notifyAdminButton = (ImageButton) v.findViewById(R.id.notifyAdmin);

        postCommentLayout = (RelativeLayout) v.findViewById(R.id.post_comment);
        shareStatusLayout = (RelativeLayout) postCommentLayout.findViewById(R.id.share_status);

        commentEditText = (EditText) shareStatusLayout.findViewById(R.id.comment_edit_text);
        commentTextCounter = (TextView) shareStatusLayout.findViewById(R.id.comment_text_counter);
        postCommentButton = (ImageButton) shareStatusLayout.findViewById(R.id.comment_button);

        postCommentLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                toggleCommentViewVisibility(parentFeedData);
                return false;
            }
        });

        feedArrayList = OpenUpServiceManager.getInstance().getFeedArray();
        if (feedArrayList.size()==0)
            loadFeed();
        else
            listadapter.updateList(feedArrayList);

        notifyAdminButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyAdminButton.setSelected(!notifyAdminButton.isSelected());
            }
        });

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (composeEditText.getText().toString().length()>0)
                {
                    OpenUpServiceManager.getInstance().postStatus(composeEditText.getText().toString(),notifyAdminButton.isSelected(),new JsonHttpResponseHandler()
                    {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);

                            if (response.has("feed"))
                            {
                                try {
                                    OpenUpServiceManager.getInstance().getFeedArray().add(0,response.getJSONObject("feed"));
                                    listadapter.updateList(OpenUpServiceManager.getInstance().getFeedArray());
                                    Utility.hideKeyboard(getActivity());
                                    composeEditText.setText("");
                                    textCounterText.setText("140");
                                    notifyAdminButton.setSelected(false);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                }
                else
                {
                    Utility.showAlertDialog(getActivity(), getResources().getString(R.string.enter_message));
                }
            }
        });

        postCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentEditText.getText().toString().length()>0)
                {
                    try {
                    OpenUpServiceManager.getInstance().postComment(commentEditText.getText().toString(), Integer.parseInt(parentFeedData.getString("post_id")), new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);

                            if (response.has("feed")) {

//                                    OpenUpServiceManager.getInstance().getFeedArray().add(0, response.getJSONObject("feed"));
//                                    listadapter.updateList(OpenUpServiceManager.getInstance().getFeedArray());
                                    Utility.hideKeyboard(getActivity());
                                    toggleCommentViewVisibility(parentFeedData);
//                                    composeEditText.setText("");
//                                    textCounterText.setText("140");
//                                    notifyAdminButton.setSelected(false);

                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Utility.showAlertDialog(getActivity(), getResources().getString(R.string.enter_message));
                }
            }
        });

        composeEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                textCounterText.setText(String.valueOf(140 - composeEditText.getText().toString().length()));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        commentEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                commentTextCounter.setText(String.valueOf(140 - commentEditText.getText().toString().length()));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int lastInScreen = firstVisibleItem + visibleItemCount;
                if((lastInScreen == totalItemCount) && !loadingMore)
                {
                    loadNextPage();
                }
                else
                {
                    loadingMore = false;
                }
            }
        });
        return v;
    }

    private void loadFeed()
    {
        OpenUpServiceManager.getInstance().fetchFeed(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                feedArrayList = OpenUpServiceManager.getInstance().getFeedArray();
                listadapter.updateList(feedArrayList);
                if (listadapter.getCount() > 0) {
//                    refreshUI(mPagerAdapter.getItem(currentPage));
//                    noPostText.setVisibility(View.GONE);
                } else {
//                    noPostText.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                if (listadapter.getCount() > 0) {
//                    refreshUI(mPagerAdapter.getItem(currentPage));
//                    noPostText.setVisibility(View.GONE);
                } else {
//                    noPostText.setVisibility(View.VISIBLE);
                }

            }

        });
    }

    private void loadNextPage()
    {
        if (OpenUpServiceManager.getInstance().getCurrentPage()<OpenUpServiceManager.getInstance().getTotalPagePage())
        {
            OpenUpServiceManager.getInstance().setCurrentPage(OpenUpServiceManager.getInstance().getCurrentPage()+1);
            loadingMore = true;
            loadFeed();
        }

    }

    public void toggleCommentViewVisibility(JSONObject feedData)
    {
        parentFeedData = feedData;
        if (postCommentLayout.getVisibility()==View.VISIBLE) {
            postCommentLayout.setVisibility(View.GONE);
            commentEditText.setText("");
            commentTextCounter.setText("140");
            listView.setEnabled(true);
        }
        else {
            postCommentLayout.setVisibility(View.VISIBLE);
            listView.setEnabled(false);
        }
    }

    private void showDetailView(JSONObject feedData)
    {
            LayoutInflater layoutInflater =
                    (LayoutInflater) this.getActivity().getSystemService(this.getActivity().LAYOUT_INFLATER_SERVICE);
            final FeedDetailItem feedDetailItem = (FeedDetailItem) layoutInflater.inflate(R.layout.feed_detail_item, null);


            final RelativeLayout rootView = (RelativeLayout) this.getView().findViewById(R.id.root_view);
        rootView.addView(feedDetailItem, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

            feedDetailItem.setData(feedData, this.getActivity(), false);

    }
}