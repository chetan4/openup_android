package com.idyllic_software.openup.view;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.idyllic_software.openup.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by javalnanda on 23/04/15.
 */
public class FeedItem extends RelativeLayout {

    private TextView feedDescriptionTextView;
    private TextView feedTimeStamp;

    public FeedItem(Context context) {
        super(context);
    }

    public FeedItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public FeedItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        feedDescriptionTextView = (TextView) findViewById(R.id.feed_text);
        feedTimeStamp= (TextView) findViewById(R.id.feed_timestamp_text);

    }

    private int getCurrentTimezoneOffset() {

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        int offsetInMillis = tz.getOffset(cal.getTimeInMillis());
        return offsetInMillis;
    }

    public void setData(JSONObject feed)
    {
        if (feed!=null)
        {
            try {
                feedDescriptionTextView.setText(feed.getString("content"));

                try {
                    String dateStr = feed.getString("created_at");


                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());

//                    Calendar cal = Calendar.getInstance();
//                    TimeZone tz = cal.getTimeZone();


                    dateFormat.setTimeZone(TimeZone.getDefault());
                    Date convertedDate = new Date();
                    try {
                        convertedDate = dateFormat.parse(dateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String relativeTime = null;
                    try {
                        long time = convertedDate.getTime() + getCurrentTimezoneOffset();
                        relativeTime = (String) DateUtils.getRelativeTimeSpanString(time);
//                        relativeTime = (String) DateUtils.getRelativeDateTimeString(getContext(), time, DateUtils.SECOND_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    //Log.i(TAG, relativeTime);
                    feedTimeStamp.setText(relativeTime);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
