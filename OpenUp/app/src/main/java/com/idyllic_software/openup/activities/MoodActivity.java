package com.idyllic_software.openup.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.idyllic_software.openup.utils.Utility;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

/**
 * Created by javalnanda on 21/04/15.
 */
public class MoodActivity extends Activity {

//    SeekBar moodSeekBar;
//    Button proceedButton;
    ImageView currentMoodImage;
    ImageButton moodSad,moodNeutral,moodHappy;
    int moodValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mood);

        currentMoodImage = (ImageView) findViewById(R.id.current_mood_image);

        moodSad = (ImageButton) findViewById(R.id.mood_sad);
        moodNeutral = (ImageButton) findViewById(R.id.mood_neutral);
        moodHappy = (ImageButton) findViewById(R.id.mood_happy);

        moodSad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMoodImage.setImageDrawable(getResources().getDrawable(R.drawable.sad));
                moodSad.setSelected(true);
                moodNeutral.setSelected(false);
                moodHappy.setSelected(false);
//                proceedButton.setEnabled(true);
                moodValue = 25;
                updateMood();
            }
        });

        moodNeutral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMoodImage.setImageDrawable(getResources().getDrawable(R.drawable.neutral));
//                proceedButton.setEnabled(true);
                moodSad.setSelected(false);
                moodNeutral.setSelected(true);
                moodHappy.setSelected(false);
                moodValue = 50;
                updateMood();
            }
        });

        moodHappy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentMoodImage.setImageDrawable(getResources().getDrawable(R.drawable.happy));
//                proceedButton.setEnabled(true);
                moodSad.setSelected(false);
                moodNeutral.setSelected(false);
                moodHappy.setSelected(true);
                moodValue = 75;
                updateMood();
            }
        });

//        proceedButton = (Button) findViewById(R.id.proceed_button);
//        proceedButton.setEnabled(false);
//        proceedButton.setTextColor(getResources().getColor(R.color.menu_text_color));
//
//        proceedButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int moodValue = moodSeekBar.getProgress();
//
//                if(moodValue<=0)
//                    moodValue = 1;


//            }
//        });

        /*moodSeekBar = (SeekBar) findViewById(R.id.mood_seekbar);
        moodSeekBar.setProgress(60);
        moodSeekBar.setMax(100);

        moodSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                //add here your implementation
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                //add here your implementation
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {

                //Enable proceed only when user changes mood on slider
                proceedButton.setEnabled(true);
                proceedButton.setTextColor(getResources().getColor(R.color.white_text_color));
                //add here your implementation


                if (progress <= 25) {
                    currentMoodImage.setImageDrawable(getResources().getDrawable(R.drawable.very_sad));
                } else if(progress > 25 && progress <= 50)
                {
                    currentMoodImage.setImageDrawable(getResources().getDrawable(R.drawable.sad));
                }
                else if(progress > 50 && progress <= 75)
                {
                    currentMoodImage.setImageDrawable(getResources().getDrawable(R.drawable.happy));
                }
                else
                {
                    currentMoodImage.setImageDrawable(getResources().getDrawable(R.drawable.very_happy));
                }
            }
        });*/
    }

    private void updateMood() {
        OpenUpServiceManager.getInstance().initialize(MoodActivity.this);
        OpenUpServiceManager.getInstance().updateMood(moodValue, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                Utility.updateMoodDateInPreference(MoodActivity.this);

                Intent intent = new Intent(MoodActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }
}
