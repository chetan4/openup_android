package com.idyllic_software.openup.fragments;

import android.support.v4.app.Fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.adapters.EmailListAdapter;
import com.idyllic_software.openup.view.EmailItem;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.idyllic_software.openup.utils.Utility;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by javalnanda on 22/04/15.
 */
public class InviteFragment extends Fragment implements SearchView.OnQueryTextListener {

    public InviteFragment(){}

    private ArrayList<EmailItem> list = null;
    private ArrayList<EmailItem> filterList = null;

    private ListView listView;
    private EmailListAdapter listadapter;
    private EditText emailEditText;
    private Button inviteButton;
    private TextWatcher mSearchTw;
    private SearchView mSearchView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_invite, container, false);

        list = getData();

        mSearchView = (SearchView) rootView.findViewById(R.id.search_view);

        listView = (ListView) rootView.findViewById(R.id.email_listview);
        listadapter = new EmailListAdapter(getActivity(), R.layout.email_list_item, list);
        listView.setAdapter(listadapter);

        listView.setOnItemClickListener(new EmailListClickListener());

//        listView.setTextFilterEnabled(true);

        setupSearchView();

        emailEditText = (EditText) rootView.findViewById(R.id.email_editText);
        inviteButton = (Button) rootView.findViewById(R.id.button_invite);

        /*emailEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                filterList(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

*/

        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.validEmail(emailEditText.getText().toString()))
                {
                    OpenUpServiceManager.getInstance().sendInvite(emailEditText.getText().toString(),new JsonHttpResponseHandler()
                    {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);

                            Utility.showAlertDialog(getActivity(),getString(R.string.invite_success));
                            emailEditText.setText("");
                            emailEditText.clearComposingText();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                }
                else
                {
                    Utility.showAlertDialog(getActivity(),getString(R.string.enter_valid_email));
                }
            }
        });
        return rootView;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public boolean onQueryTextChange(String newText) {
        android.widget.Filter filter = listadapter.getFilter();

//        if (TextUtils.isEmpty(newText)) {
//            listView.clearTextFilter();
//        } else {
//            listView.setFilterText(newText.toString());
            filter.filter(newText.toString());
//        }
        return true;
    }

    private void setupSearchView() {
        mSearchView.setIconifiedByDefault(false);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setSubmitButtonEnabled(false);
        mSearchView.setQueryHint("Search Here");
    }

    private void filterList(CharSequence s)
    {
        filterList = new ArrayList<EmailItem>();
        for (int i=0;i<list.size();i++)
        {
            if (list.get(i).getKey().contains(s) || list.get(i).getValue().contains(s))
            {
                filterList.add(list.get(i));
            }
        }

        if (filterList.size()>0)
        {
            listadapter.updateList(filterList);
        }
        else
        {
            listadapter.updateList(list);
        }
    }
    /**
     * Slide menu item click listener
     * */
    private class EmailListClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            selectEmail(position);
        }
    }

    private void selectEmail(int position)
    {
        emailEditText.setText(listadapter.getItem(position).getValue());
    }
    private ArrayList<EmailItem> getData() {
        ArrayList<EmailItem> accountsList = new ArrayList<EmailItem>();

        //Getting all registered Google Accounts;
        try {


            Context context = getActivity();
            ContentResolver cr = context.getContentResolver();
            String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.PHOTO_ID,
                    ContactsContract.CommonDataKinds.Email.DATA,
                    ContactsContract.CommonDataKinds.Photo.CONTACT_ID };
            String order = "CASE WHEN "
                    + ContactsContract.Contacts.DISPLAY_NAME
                    + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                    + ContactsContract.Contacts.DISPLAY_NAME
                    + ", "
                    + ContactsContract.CommonDataKinds.Email.DATA
                    + " COLLATE NOCASE";
            String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
            Cursor cur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, filter, null, order);
            if (cur.moveToFirst()) {
                do {
                    // names comes in hand sometimes
                    String name = cur.getString(1);
                    String emlAddr = cur.getString(3);

                    // keep unique only
                    EmailItem item = new EmailItem( name, emlAddr);
                        accountsList.add(item);
                } while (cur.moveToNext());
            }

            cur.close();

        } catch (Exception e) {
            Log.i("Exception", "Exception:" + e);
        }

        return accountsList;
    }
}