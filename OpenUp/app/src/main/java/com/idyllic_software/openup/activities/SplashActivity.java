package com.idyllic_software.openup.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.utils.OpenUpConstants;
import com.idyllic_software.openup.utils.SharedPreferenceManager;
import com.idyllic_software.openup.utils.Utility;

/**
 * Created by javalnanda on 21/04/15.
 */
public class SplashActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

         /*
          * Showing splash screen with a timer. This will be useful when you
          * want to show case your app logo / company
          */

            @Override
            public void run() {
                // This method will be executed once the timer is over

                if(SharedPreferenceManager.readPreferences(SplashActivity.this,OpenUpConstants.USER_TOKEN_PREFERENCES,"")!="" )
                {

                    // Start your app main activity
                    if (!Utility.isMoodUpdatedToday(SplashActivity.this))
                    {
                        Intent i = new Intent(SplashActivity.this, MoodActivity.class);
                        startActivity(i);
                    }
                    else
                    {
                        Intent i = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(i);
                    }

                }
                else
                {
                    // Show register activity
                    Intent i = new Intent(SplashActivity.this, RegisterActivity.class);
                    startActivity(i);

                }

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}