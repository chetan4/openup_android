package com.idyllic_software.openup.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.fragments.ActivitiesFeedFragment;
import com.idyllic_software.openup.fragments.TrendingFeedFragment;
import com.idyllic_software.openup.view.ActivityItem;
import com.idyllic_software.openup.view.TrendingItem;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by javalnanda on 30/04/15.
 */
public class TrendingListAdapter extends ArrayAdapter<JSONObject>{
    private List<JSONObject> itemList = null;
    private Context context;
    private TrendingFeedFragment parentFragment;

    public TrendingListAdapter(Context context, int textViewResourceId, List<JSONObject> itemList, TrendingFeedFragment parentFragment) {
        super(context, textViewResourceId, itemList);
        this.context = context;
        this.itemList = itemList;
        this.parentFragment = parentFragment;

    }

    public void updateList(List<JSONObject> updatedList)
    {
        this.itemList = updatedList;
        this.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
//        return ((null != itemList) ? itemList.size() : 0);
        return 10;
    }

    @Override
    public JSONObject getItem(int position) {
        return ((null != itemList) ? itemList.get(position) : null);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (null == view) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.trending_item, null);
        }


        if (view != null) {
            TrendingItem activityItem = (TrendingItem)view;
            activityItem.setParentFragment(parentFragment);
            activityItem.setData(getItem(position), this.context);
        }
//        TrendingItem data = itemList.get(position);
//        if (null != data) {
//
//            TextView name = (TextView) view.findViewById(R.id.name_text);
//            TextView email = (TextView) view.findViewById(R.id.email_text);
//
//            name.setText(data.getKey());
//            email.setText(data.getValue());
//        }
        return view;
    }
}

