package com.idyllic_software.openup.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.idyllic_software.openup.utils.OpenUpConstants;
import com.idyllic_software.openup.utils.SharedPreferenceManager;
import com.idyllic_software.openup.utils.Utility;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by javalnanda on 21/04/15.
 */
public class VerifyActivity extends Activity {

    private EditText pinEditText;
    private Button verifyButton;

    TextView pinBox0;
    TextView pinBox1;
    TextView pinBox2;
    TextView pinBox3;

    TextView [] pinBoxArray;

    LinearLayout pinContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);

        pinBox0 = (TextView)findViewById(R.id.pinBox0);
        pinBox1 = (TextView)findViewById(R.id.pinBox1);
        pinBox2 = (TextView)findViewById(R.id.pinBox2);
        pinBox3 = (TextView)findViewById(R.id.pinBox3);

        pinContainer = (LinearLayout) findViewById(R.id.pin_container);

        pinEditText = (EditText) findViewById(R.id.pin_editText);
        pinEditText.setMaxWidth(pinEditText.getWidth());
        pinEditText.setMaxHeight(pinEditText.getHeight());

        pinEditText.setLayoutParams(pinContainer.getLayoutParams());
        verifyButton = (Button) findViewById(R.id.verifyButton);

        pinEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                switch (s.length())
                {
                    case 1:
                        pinBox0.setText(s.subSequence(0,1));
                        pinBox1.setText("");
                        pinBox2.setText("");
                        pinBox3.setText("");
                        break;
                    case 2:
                        pinBox0.setText(s.subSequence(0,1));
                        pinBox1.setText(s.subSequence(1,2));
                        pinBox2.setText("");
                        pinBox3.setText("");
                        break;
                    case 3:
                        pinBox0.setText(s.subSequence(0,1));
                        pinBox1.setText(s.subSequence(1,2));
                        pinBox2.setText(s.subSequence(2,3));
                        pinBox3.setText("");
                        break;
                    case 4:
                        pinBox0.setText(s.subSequence(0,1));
                        pinBox1.setText(s.subSequence(1,2));
                        pinBox2.setText(s.subSequence(2,3));
                        pinBox3.setText(s.subSequence(3,4));
                        Utility.hideKeyboard(VerifyActivity.this);
                        break;
                    default:
                        pinBox0.setText("0");
                        pinBox1.setText("0");
                        pinBox2.setText("0");
                        pinBox3.setText("0");
                        break;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        verifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (pinEditText.getText().toString().length()<4)
                {
                    Utility.showAlertDialog(VerifyActivity.this,getResources().getString(R.string.enter_pin));
                }
                else
                {
                    OpenUpServiceManager.getInstance().initialize(VerifyActivity.this);
                    OpenUpServiceManager.getInstance().verifyUser(pinEditText.getText().toString(), new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                            super.onSuccess(statusCode, headers, responseString);
                        }

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);

                            String userToken = null;
                            try {
                                userToken = response.getString("user_token");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if(userToken!=null)
                            {
                                // Navigate to main screen
                                SharedPreferenceManager.savePreferences(VerifyActivity.this, OpenUpConstants.USER_TOKEN_PREFERENCES,userToken);
                                Intent i = new Intent(VerifyActivity.this, MainActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                                finish();
                            }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            super.onFailure(statusCode, headers, responseString, throwable);
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }

                    });


                }
            }
        });
    }

    private void updateText(CharSequence s)
    {
        if(s.length()==0)
        {

        }
    }
}