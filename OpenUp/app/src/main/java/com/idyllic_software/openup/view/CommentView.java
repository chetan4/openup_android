package com.idyllic_software.openup.view;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.idyllic_software.openup.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by javalnanda on 23/04/15.
 */
public class CommentView extends RelativeLayout {


    public CommentView(Context context) {
        super(context);
    }

    public CommentView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CommentView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        getLayoutParams().width = LayoutParams.MATCH_PARENT;
        getLayoutParams().height= LayoutParams.MATCH_PARENT;

    }
}
