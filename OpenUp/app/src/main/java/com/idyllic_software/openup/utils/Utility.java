package com.idyllic_software.openup.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.format.DateUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.Date;
import java.util.regex.Pattern;

/**
 * Created by javalnanda on 21/04/15.
 */
public class Utility {

    public static void showAlertDialog(Context context, String message)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Open Up")
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("OK",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showAlertDialogWithCancelListener(Context context, String message, final DialogInterface.OnCancelListener cancelListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Open Up")
                .setMessage(message)
                .setCancelable(false)
                .setNegativeButton("OK", (DialogInterface.OnClickListener) cancelListener);
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static boolean isMoodUpdatedToday(Activity context)
    {
       return DateUtils.isToday(SharedPreferenceManager.readLondValue(context,OpenUpConstants.MOOD_UPDATE_DATE_PREFERENCES, (long) 0));
    }

    public static void updateMoodDateInPreference(Activity context)
    {
        //getting the current time in milliseconds, and creating a Date object from it:
        Date date = new Date(System.currentTimeMillis());
        SharedPreferenceManager.saveLongValue(context,OpenUpConstants.MOOD_UPDATE_DATE_PREFERENCES,date.getTime());
    }

    public static void hideKeyboard(Activity context) {
        // Check if no view has focus:
        View view = context.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void showKeyboard(Activity context,EditText editText)
    {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }
}
