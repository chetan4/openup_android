package com.idyllic_software.openup.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.idyllic_software.openup.R;

/**
 * Created by javalnanda on 22/04/15.
 */
public class ContentWebviewFragment extends Fragment {

    private  WebView webview;
    private String urlString;
    public ContentWebviewFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_content_webview, container, false);

        webview =(WebView) rootView.findViewById(R.id.content_webview);
        webview.loadUrl(urlString);
        return rootView;
    }

    public void setUrlString(String urlString)
    {
        this.urlString = urlString;
    }

    @Override
    public void onStart()
    {
        super.onStart();

    }

}