package com.idyllic_software.openup.view;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.fragments.ActivitiesFeedFragment;
import com.idyllic_software.openup.fragments.TrendingFeedFragment;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by javalnanda on 23/04/15.
 */
public class TrendingItem extends RelativeLayout {

    private TextView feedDescriptionTextView;
    private TextView feedTimeStamp;
    private TextView upvoteCount;
    private TextView downvoteCount;
    private Button commentCount;

    private ImageButton followButton;
    private Context context;
    private JSONObject feedData;
    private TrendingFeedFragment parentFragment;

    public TrendingItem(Context context) {
        super(context);
    }

    public TrendingItem(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public TrendingItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        feedDescriptionTextView = (TextView) findViewById(R.id.feed_text);
        feedTimeStamp= (TextView) findViewById(R.id.feed_timestamp_text);
        upvoteCount= (TextView) findViewById(R.id.upvote_count);
        downvoteCount= (TextView) findViewById(R.id.downvote_count);
        commentCount= (Button) findViewById(R.id.comment_count);

        followButton = (ImageButton) findViewById(R.id.follow_button);

        followButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    OpenUpServiceManager.getInstance().followPost(Integer.parseInt(feedData.getString("post_id")), new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            if (response.has("feed")) {
                                try {
                                    feedData = response.getJSONObject("feed");
                                    setData(feedData,context);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private int getCurrentTimezoneOffset() {

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        int offsetInMillis = tz.getOffset(cal.getTimeInMillis());
        return offsetInMillis;
    }

    public void setParentFragment(TrendingFeedFragment parentFragment)
    {
        this.parentFragment = parentFragment;
    }

    public void setData(JSONObject feed, Context context)
    {
        this.context = context;
        if (feed!=null)
        {
            try {
                feedData = feed;
                feedDescriptionTextView.setText(feed.getString("content"));

                try {
                    String dateStr = feed.getString("created_at");


                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());

//                    Calendar cal = Calendar.getInstance();
//                    TimeZone tz = cal.getTimeZone();


                    dateFormat.setTimeZone(TimeZone.getDefault());
                    Date convertedDate = new Date();
                    try {
                        convertedDate = dateFormat.parse(dateStr);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    String relativeTime = null;
                    try {
                        long time = convertedDate.getTime() + getCurrentTimezoneOffset();
                        relativeTime = (String) DateUtils.getRelativeTimeSpanString(time);
//                        relativeTime = (String) DateUtils.getRelativeDateTimeString(getContext(), time, DateUtils.SECOND_IN_MILLIS, DateUtils.WEEK_IN_MILLIS, DateUtils.FORMAT_ABBREV_ALL);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    //Log.i(TAG, relativeTime);
                    feedTimeStamp.setText(relativeTime);

                    if (Integer.parseInt(feed.getString("upvote_count"))>0) {
                        String appendString;
                        if (Integer.parseInt(feed.getString("downvote_count"))>0 || (Integer.parseInt(feed.getString("comments_count"))>0))
                        {
                            if(Integer.parseInt(feed.getString("upvote_count"))>1)
                                appendString = " UpVotes | ";
                            else
                                appendString = " UpVote | ";
                        }
                        else
                        {
                            if(Integer.parseInt(feed.getString("upvote_count"))>1)
                                appendString = " UpVotes ";
                            else
                                appendString = " UpVote ";
                        }
                        upvoteCount.setText(feed.getString("upvote_count") + appendString);
                    }
                    else
                        upvoteCount.setVisibility(View.GONE);

                    if (Integer.parseInt(feed.getString("downvote_count"))>0)
                    {
                        String appendString;
                        if (Integer.parseInt(feed.getString("comments_count"))>0)
                        {
                            if(Integer.parseInt(feed.getString("downvote_count"))>1)
                                appendString = " DownVotes | ";
                            else
                                appendString = " DownVote | ";
                        }
                        else
                        {
                            if(Integer.parseInt(feed.getString("downvote_count"))>1)
                                appendString = " DownVotes ";
                            else
                                appendString = " DownVote ";
                        }
                        downvoteCount.setText(feed.getString("downvote_count") + appendString);
                    }
                    else
                        downvoteCount.setVisibility(GONE);

                    if (Integer.parseInt(feed.getString("comments_count"))>0)
                    {
                        if(Integer.parseInt(feed.getString("comments_count"))>1)
                            commentCount.setText(feed.getString("comments_count") + "Comments");
                        else
                            commentCount.setText(feed.getString("comments_count") + "Comment");
                    }
                    else
                        commentCount.setVisibility(GONE);

                    followButton.setSelected(Boolean.parseBoolean(feed.getString("user_followed")));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
