package com.idyllic_software.openup.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.view.FeedItem;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by javalnanda on 23/04/15.
 */
public class ViewPagerAdapter extends PagerAdapter {

    private ArrayList<JSONObject> data;
    private Context mContext;
    LayoutInflater mLayoutInflater;

    public ViewPagerAdapter(Context ctx, ArrayList<JSONObject> data) {
        this.mContext = ctx;
        this.data = data;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void updateFeed(ArrayList<JSONObject> feedArray)
    {
        this.data = feedArray;
        notifyDataSetChanged();
    }

    public JSONObject getItem(int index)
    {
        return data.get(index);
    }

    @Override
    public int getCount() {
       return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.feed_item, container, false);

        if (itemView != null) {
            FeedItem feedItem = (FeedItem) itemView.findViewById(R.id.view_feed_item);
            if (feedItem != null) {
                feedItem.setData(getItem(position));
            }
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FeedItem)object);
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}