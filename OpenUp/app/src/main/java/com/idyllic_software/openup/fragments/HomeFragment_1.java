package com.idyllic_software.openup.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.adapters.ViewPagerAdapter;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by javalnanda on 22/04/15.
 */
public class HomeFragment_1 extends Fragment {

    private static final int NUM_PAGES = 5;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private ViewPagerAdapter mPagerAdapter;

    private ImageButton upVoteButton;
    private TextView upVoteCount;

    private ImageButton downVoteButton;
    private TextView downVoteCount;

    private ImageButton flagButton;

    private int currentPage=0;

    private ImageView leftArrow;
    private ImageView rightArrow;

    private TextView noPostText;

    public HomeFragment_1(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home_1, container, false);

        upVoteButton = (ImageButton) rootView.findViewById(R.id.upvote_button);
        upVoteCount = (TextView) rootView.findViewById(R.id.upvote_count_text);

        downVoteButton = (ImageButton) rootView.findViewById(R.id.downvote_button);
        downVoteCount = (TextView) rootView.findViewById(R.id.downvote_count_text);

        flagButton = (ImageButton) rootView.findViewById(R.id.flag_button);

        leftArrow = (ImageView) rootView.findViewById(R.id.left_arrow);
        rightArrow = (ImageView) rootView.findViewById(R.id.right_arrow);

        noPostText = (TextView) rootView.findViewById(R.id.no_post_text);

        upVoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    OpenUpServiceManager.getInstance().upVotePost(Integer.parseInt(mPagerAdapter.getItem(currentPage).getString("post_id")), new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            if (response.has("feed"))
                            {
                                try {
                                    refreshCurrentFeed(response.getJSONObject("feed"));
                                    refreshUI(mPagerAdapter.getItem(currentPage));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        downVoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    OpenUpServiceManager.getInstance().downVotePost(Integer.parseInt(mPagerAdapter.getItem(currentPage).getString("post_id")), new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            if (response.has("feed"))
                            {
                                try {
                                    refreshCurrentFeed(response.getJSONObject("feed"));
                                    refreshUI(mPagerAdapter.getItem(currentPage));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        flagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    OpenUpServiceManager.getInstance().flagPost(Integer.parseInt(mPagerAdapter.getItem(currentPage).getString("post_id")), new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            if (response.has("feed"))
                            {
                                try {
                                    refreshCurrentFeed(response.getJSONObject("feed"));
                                    refreshUI(mPagerAdapter.getItem(currentPage));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        mPager = (ViewPager) rootView.findViewById(R.id.post_viewpager);
        mPagerAdapter = new ViewPagerAdapter(getActivity(),OpenUpServiceManager.getInstance().getFeedArray());
        mPager.setAdapter(mPagerAdapter);
        mPagerAdapter.notifyDataSetChanged();

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                currentPage = position;


            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state==ViewPager.SCROLL_STATE_IDLE)
                {
                    if (currentPage==mPagerAdapter.getCount()-1
                            && (OpenUpServiceManager.getInstance().getCurrentPage()<OpenUpServiceManager.getInstance().getTotalPagePage()))
                    {
                        loadNextPage();
                    }

                    try {
                        if (mPagerAdapter.getCount()>0)
                        {
                            refreshUI(mPagerAdapter.getItem(currentPage));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        if (OpenUpServiceManager.getInstance().getFeedArray().size()==0)
            loadFeed();

        refreshArrowVisibility();

        return rootView;
    }

    private void refreshArrowVisibility()
    {
        if (mPagerAdapter.getCount()==0)
        {
            leftArrow.setVisibility(View.GONE);
            rightArrow.setVisibility(View.GONE);
        }

        if (currentPage==0)
        {
            leftArrow.setVisibility(View.GONE);
        }

        if (mPagerAdapter.getCount()>0 && currentPage == mPagerAdapter.getCount()-1)
        {
            rightArrow.setVisibility(View.GONE);
        }

        if (mPagerAdapter.getCount()>0 && (currentPage>0 && currentPage < mPagerAdapter.getCount()-1))
        {
            leftArrow.setVisibility(View.VISIBLE);
            rightArrow.setVisibility(View.VISIBLE);
        }

    }
    private void loadFeed()
    {
        OpenUpServiceManager.getInstance().fetchFeed(new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                mPagerAdapter.updateFeed(OpenUpServiceManager.getInstance().getFeedArray());
                if (mPagerAdapter.getCount()>0) {
                    refreshUI(mPagerAdapter.getItem(currentPage));
                    noPostText.setVisibility(View.GONE);
                }
                else
                {
                    noPostText.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                if (mPagerAdapter.getCount()>0) {
                    refreshUI(mPagerAdapter.getItem(currentPage));
                    noPostText.setVisibility(View.GONE);
                }
                else
                {
                    noPostText.setVisibility(View.VISIBLE);
                }

            }

        });
    }

    private void loadNextPage()
    {
        OpenUpServiceManager.getInstance().setCurrentPage(OpenUpServiceManager.getInstance().getCurrentPage()+1);
        loadFeed();
    }

    public void updateFeedOnStatusPosted()
    {
        mPagerAdapter.updateFeed(OpenUpServiceManager.getInstance().getFeedArray());
    }

    public void refreshCurrentFeed(JSONObject feed)
    {
        OpenUpServiceManager.getInstance().getFeedArray().remove(currentPage);
        OpenUpServiceManager.getInstance().getFeedArray().add(currentPage,feed);
        mPagerAdapter.updateFeed(OpenUpServiceManager.getInstance().getFeedArray());
    }

    public void refreshEntireContent()
    {
        OpenUpServiceManager.getInstance().setCurrentPage(1);
        loadFeed();
    }

    private void refreshUI(JSONObject feedItem)
    {

        try {
            upVoteCount.setText(feedItem.getString("upvote_count"));
            downVoteCount.setText(feedItem.getString("downvote_count"));

            flagButton.setSelected(Boolean.parseBoolean(feedItem.getString("user_flagged")));

            upVoteButton.setSelected(Boolean.parseBoolean(feedItem.getString("user_upvoted")));
            downVoteButton.setSelected(Boolean.parseBoolean(feedItem.getString("user_downvoted")));

            refreshArrowVisibility();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}