package com.idyllic_software.openup.services;

/**
 * Created by javalnanda on 23/04/15.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Message;
import android.util.Log;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.activities.RegisterActivity;
import com.idyllic_software.openup.utils.OpenUpConstants;
import com.idyllic_software.openup.utils.ProgressHUD;
import com.idyllic_software.openup.utils.SharedPreferenceManager;
import com.idyllic_software.openup.utils.Utility;
import com.loopj.android.http.*;

import org.apache.http.Header;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OpenUpServiceManager {

    private static AsyncHttpClient serviceClient = new AsyncHttpClient();
    private Context context;
    private static OpenUpServiceManager _instance;
    private ProgressHUD progressDialog;

    private String emailToRegister;
    private ArrayList<JSONObject> feedArray;
    private ArrayList<JSONObject> trendingArray;
    private int currentPage=1;
    private int totalPage;

    private int currentTrendingPage=1;
    private int totalPageForTrending;

    public boolean isOnline() {
        ConnectivityManager cm;
        cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            Utility.showAlertDialog(context, context.getResources().getString(R.string.no_internet));
        }
        return false;
    }

    public static OpenUpServiceManager getInstance() {
        if (_instance == null) {
            _instance = new OpenUpServiceManager();
        }
        return _instance;
    }

    public void initialize(Context context)
    {
        this.context = context;
//        progressDialog = ProgressHUD.construct(context,"Loading",true,false,null);
        progressDialog = ProgressHUD.ctor(context);
        feedArray = new ArrayList<JSONObject>();
    }


    public void setEmailToRegister(String email)
    {
        emailToRegister = email;
    }

    public String getEmailToRegister()
    {
        return emailToRegister;
    }

    private static String getServiceAbsoluteUrl(String relativeUrl) {
        return OpenUpConstants.BASE_URL + relativeUrl;
    }

    public void setCurrentPage(int currentPage)
    {
        this.currentPage = currentPage;
    }

    public int getCurrentPage()
    {
        return this.currentPage;
    }

    public void setCurrentTrendingPage(int currentTrendingPage)
    {
        this.currentTrendingPage = currentTrendingPage;
    }

    public int getCurrentPageForTrending()
    {
        return this.currentTrendingPage;
    }

    public int getTotalPagePage()
    {
        return this.totalPage;
    }
    public int getTotalPageForTrending()
    {
        return this.totalPageForTrending;
    }

    private String getUserToken()
    {
        return SharedPreferenceManager.readPreferences((Activity)context,OpenUpConstants.USER_TOKEN_PREFERENCES,"");
    }

    public ArrayList<JSONObject> getFeedArray()
    {
        return this.feedArray;
    }

    public ArrayList<JSONObject> getTrendingArray()
    {
        return this.trendingArray;
    }

    public void fetchFeed(final JsonHttpResponseHandler responseHandler)
    {
        RequestParams getFeedParam = new RequestParams();
        getFeedParam.add("user_token",getUserToken());
        getFeedParam.add("page", String.valueOf(currentPage));
        sendAsyncGetRequestToMethod(OpenUpConstants.GET_FEED_METHOD_NAME,getFeedParam,new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                if (response.has("feed"))
                {
                    try {
                        JSONArray array = response.getJSONArray("feed");

                        // Reset array if its first page. Triggered in case of refresh
                        if (currentPage==1)
                        {
                            feedArray = new ArrayList<JSONObject>();
                        }
                        totalPage = response.getJSONObject("page").getInt("total_page");

                        if (array != null) {
                            for (int i=0;i<array.length();i++){
                                feedArray.add(array.getJSONObject(i));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                if (response.has("page"))
                {
                    try {
                        totalPage = response.getJSONObject("page").getInt("total_page");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                responseHandler.onSuccess(statusCode, headers, response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                responseHandler.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    public void fetchTrending(final JsonHttpResponseHandler responseHandler)
    {
        RequestParams getFeedParam = new RequestParams();
        getFeedParam.add("user_token",getUserToken());
        getFeedParam.add("trending","TRUE");
        getFeedParam.add("page", String.valueOf(currentTrendingPage));
        sendAsyncGetRequestToMethod(OpenUpConstants.GET_FEED_METHOD_NAME,getFeedParam,new JsonHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                if (response.has("feed"))
                {
                    try {
                        JSONArray array = response.getJSONArray("feed");

                        // Reset array if its first page. Triggered in case of refresh
                        if (currentTrendingPage==1)
                        {
                            trendingArray = new ArrayList<JSONObject>();
                        }
                        totalPageForTrending = response.getJSONObject("page").getInt("total_page");

                        if (array != null) {
                            for (int i=0;i<array.length();i++){
                                trendingArray.add(array.getJSONObject(i));
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                if (response.has("page"))
                {
                    try {
                        totalPageForTrending = response.getJSONObject("page").getInt("total_page");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                responseHandler.onSuccess(statusCode, headers, response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                responseHandler.onFailure(statusCode, headers, throwable, errorResponse);
            }
        });
    }

    public void registerUser(String email, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("email",email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.REGISTER_USER_METHOD_NAME, requestParam, responseHandler);
    }

    public void verifyUser(String pin, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("email",getEmailToRegister());
            requestParam.put("pin",pin);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPutRequestToMethod(OpenUpConstants.VERIFY_USER_METHOD_NAME, requestParam, responseHandler);
    }

    public void postStatus(String message,Boolean notifyAdmin, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("user_token",getUserToken());
            requestParam.put("content",message);
            requestParam.put("notify_admin",notifyAdmin);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.POST_STATUS_METHOD_NAME, requestParam, responseHandler);
    }

    public void postComment(String message,int postId, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("user_token",getUserToken());
            requestParam.put("content",message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.COMMENT_METHOD_NAME.replace("postid",String.valueOf(postId)), requestParam, responseHandler);
    }

    public void sendInvite(String email, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("user_token",getUserToken());
            requestParam.put("email",email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.INVITE_METHOD_NAME, requestParam, responseHandler);
    }

    public void updateMood(int moodValue, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("user_token",getUserToken());
            requestParam.put("mood",moodValue);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.UPDATE_MOOD_METHOD_NAME, requestParam, responseHandler);
    }

    public void upVotePost(int postId, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("user_token",getUserToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.UPVOTE_METHOD_NAME.replace("postid",String.valueOf(postId)), requestParam, responseHandler);
    }

    public void downVotePost(int postId, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("user_token",getUserToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.DOWNVOTE_METHOD_NAME.replace("postid",String.valueOf(postId)), requestParam, responseHandler);
    }

    public void flagPost(int postId, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("user_token",getUserToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.FLAG_METHOD_NAME.replace("postid",String.valueOf(postId)), requestParam, responseHandler);
    }

    public void followPost(int postId, JsonHttpResponseHandler responseHandler)
    {
        JSONObject requestParam = new JSONObject();
        try {
            requestParam.put("user_token",getUserToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sendAsyncPostRequestToMethod(OpenUpConstants.FOLLOW_METHOD_NAME.replace("postid",String.valueOf(postId)), requestParam, responseHandler);
    }

    private void handleResponse(int statusCode, Header[] headers, JSONObject response,JsonHttpResponseHandler responseHandler)
    {
        try {
            if (response.getBoolean("success")==true)
            {
                responseHandler.onSuccess(statusCode,headers,response);
            }
            else
            {
                String errorMessage = response.getJSONObject("error").getString("data");
                if (response.getJSONObject("error").getInt("error_code")==OpenUpConstants.ERROR_CODE_INVALID_USER)
                {
                    //Remove user token
                    SharedPreferenceManager.savePreferences((Activity) context, OpenUpConstants.USER_TOKEN_PREFERENCES,"");


                    //Redirect to register screen
                    Intent intent = new Intent(context,RegisterActivity.class);
                    intent.putExtra("error_message", errorMessage);
                    context.startActivity(intent);

                    ((Activity)context).finish();

                }
                else
                {
                    if (errorMessage!=null)
                        Utility.showAlertDialog(context,errorMessage);
                }


                responseHandler.onFailure(statusCode,headers,new Throwable("failure"),response);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendAsyncGetRequestToMethod(String methodName, RequestParams params, final JsonHttpResponseHandler responseHandler) {

        serviceClient.setTimeout(60000);
        serviceClient.getHttpClient().getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);

        String urlStr = getServiceAbsoluteUrl(methodName);

        if (isOnline())
            serviceClient.get(context, urlStr, params, new JsonHttpResponseHandler() {


                @Override
                public void onStart() {
                    super.onStart();

                    if (progressDialog!=null)
                    {
//                        progressDialog.setMessage("Loading");
                        progressDialog.show();
                    }
                }

                @Override
                public void onFinish() {
                    super.onFinish();

                    if (progressDialog!=null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }
                }

                /**
                 * Returns when request succeeds
                 *
                 * @param statusCode http response status line
                 * @param headers    response headers if any
                 * @param response   parsed response if any
                 */
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode,headers,response);
                    handleResponse(statusCode,headers,response,responseHandler);

                }

                /**
                 * Returns when request succeeds
                 *
                 * @param statusCode http response status line
                 * @param headers    response headers if any
                 * @param response   parsed response if any
                 */
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode,headers,response);
                    responseHandler.onSuccess(statusCode,headers,response);
                }

                /**
                 * Returns when request failed
                 *
                 * @param statusCode    http response status line
                 * @param headers       response headers if any
                 * @param throwable     throwable describing the way request failed
                 * @param errorResponse parsed response if any
                 */
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode,headers,throwable,errorResponse);
                    responseHandler.onFailure(statusCode,headers,throwable,errorResponse);
                }

                /**
                 * Returns when request failed
                 *
                 * @param statusCode    http response status line
                 * @param headers       response headers if any
                 * @param throwable     throwable describing the way request failed
                 * @param errorResponse parsed response if any
                 */
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode,headers,throwable,errorResponse);
                    responseHandler.onFailure(statusCode,headers,throwable,errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode,headers,responseString,throwable);
                    responseHandler.onFailure(statusCode,headers,responseString,throwable);
                    Utility.showAlertDialog(context,responseString);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode,headers,responseString);
                    responseHandler.onSuccess(statusCode,headers,responseString);
                }
            });
    }

    private void sendAsyncPutRequestToMethod(String methodName, JSONObject params, final JsonHttpResponseHandler responseHandler) {

        serviceClient.setTimeout(60000);
        serviceClient.getHttpClient().getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
        String jsonStr = null;
        try {
            jsonStr = params.toString(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonStr, "UTF-8");
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        String urlStr = getServiceAbsoluteUrl(methodName);

        if (isOnline())
            serviceClient.put(context, urlStr, entity, "application/json", new JsonHttpResponseHandler() {


                @Override
                public void onStart() {
                    super.onStart();

                    if (progressDialog!=null)
                    {
//                        progressDialog.setMessage("Loading..");
                        progressDialog.show();
                    }
                }

                @Override
                public void onFinish() {
                    super.onFinish();

                    if (progressDialog!=null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }
                }

                /**
                 * Returns when request succeeds
                 *
                 * @param statusCode http response status line
                 * @param headers    response headers if any
                 * @param response   parsed response if any
                 */
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode,headers,response);
                    handleResponse(statusCode,headers,response,responseHandler);

                }

                /**
                 * Returns when request failed
                 *
                 * @param statusCode    http response status line
                 * @param headers       response headers if any
                 * @param throwable     throwable describing the way request failed
                 * @param errorResponse parsed response if any
                 */
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode,headers,throwable,errorResponse);
                    responseHandler.onFailure(statusCode,headers,throwable,errorResponse);
                }

                /**
                 * Returns when request failed
                 *
                 * @param statusCode    http response status line
                 * @param headers       response headers if any
                 * @param throwable     throwable describing the way request failed
                 * @param errorResponse parsed response if any
                 */
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode,headers,throwable,errorResponse);
                    responseHandler.onFailure(statusCode,headers,throwable,errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode,headers,responseString,throwable);
                    responseHandler.onFailure(statusCode,headers,responseString,throwable);
                    Utility.showAlertDialog(context,responseString);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode,headers,responseString);
                    responseHandler.onSuccess(statusCode,headers,responseString);
                }
            });
    }

    private void sendAsyncPostRequestToMethod(String methodName, JSONObject params, final JsonHttpResponseHandler responseHandler) {

        serviceClient.setTimeout(60000);
        serviceClient.getHttpClient().getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
        String jsonStr = null;
        try {
            jsonStr = params.toString(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity entity = null;
        try {
            entity = new StringEntity(jsonStr, "UTF-8");
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        } catch (Exception e) {
            e.printStackTrace();
        }
        String urlStr = getServiceAbsoluteUrl(methodName);

        if (isOnline())
            serviceClient.post(context, urlStr, entity, "application/json", new JsonHttpResponseHandler() {


                @Override
                public void onStart() {
                    super.onStart();

                    if (progressDialog!=null)
                    {
//                        progressDialog.setMessage("Loading..");
                        progressDialog.show();
                    }
                }

                @Override
                public void onFinish() {
                    super.onFinish();

                    if (progressDialog!=null && progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }
                }

                /**
                 * Returns when request succeeds
                 *
                 * @param statusCode http response status line
                 * @param headers    response headers if any
                 * @param response   parsed response if any
                 */
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode,headers,response);
                    handleResponse(statusCode,headers,response,responseHandler);

                }

                /**
                 * Returns when request succeeds
                 *
                 * @param statusCode http response status line
                 * @param headers    response headers if any
                 * @param response   parsed response if any
                 */
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode,headers,response);
                    responseHandler.onSuccess(statusCode,headers,response);
                }

                /**
                 * Returns when request failed
                 *
                 * @param statusCode    http response status line
                 * @param headers       response headers if any
                 * @param throwable     throwable describing the way request failed
                 * @param errorResponse parsed response if any
                 */
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode,headers,throwable,errorResponse);
                    responseHandler.onFailure(statusCode,headers,throwable,errorResponse);
                }

                /**
                 * Returns when request failed
                 *
                 * @param statusCode    http response status line
                 * @param headers       response headers if any
                 * @param throwable     throwable describing the way request failed
                 * @param errorResponse parsed response if any
                 */
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode,headers,throwable,errorResponse);
                    responseHandler.onFailure(statusCode,headers,throwable,errorResponse);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode,headers,responseString,throwable);
                    responseHandler.onFailure(statusCode,headers,responseString,throwable);
                    Utility.showAlertDialog(context,responseString);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    super.onSuccess(statusCode,headers,responseString);
                    responseHandler.onSuccess(statusCode,headers,responseString);
                }
            });
    }

}
