package com.idyllic_software.openup.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.idyllic_software.openup.R;

public class ProgressHUD extends ProgressDialog {

    private AnimationDrawable animation;
    private TextView messageText;
    public ProgressHUD(Context context) {
        super(context);
    }

    public ProgressHUD(Context context, int theme) {
        super(context, theme);
    }


    public void setMessage(CharSequence message) {
		if(message != null && message.length() > 0) {
			messageText.setText(message);
            messageText.invalidate();
		}
	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_hud);

        messageText = (TextView) findViewById(R.id.message);
        ImageView la = (ImageView) findViewById(R.id.spinnerImageView);
        animation = (AnimationDrawable) la.getBackground();;
    }


    @Override
    public void show() {
        super.show();
        animation.start();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        animation.stop();
    }

    public static ProgressHUD ctor(Context context) {
        ProgressHUD dialog = new ProgressHUD(context,R.style.ProgressHUD);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        return dialog;
    }
}
