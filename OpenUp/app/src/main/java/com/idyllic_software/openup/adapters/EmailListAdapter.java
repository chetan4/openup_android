package com.idyllic_software.openup.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.view.EmailItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by javalnanda on 30/04/15.
 */
public class EmailListAdapter extends ArrayAdapter<EmailItem> implements Filterable{
    private List<EmailItem> appsList = null;
    private List<EmailItem> orig = null;
    private Context context;

    public EmailListAdapter(Context context, int textViewResourceId, List<EmailItem> appsList) {
        super(context, textViewResourceId, appsList);
        this.context = context;
        this.appsList = appsList;
        this.orig = appsList;
    }

    public void updateList(List<EmailItem> updatedList)
    {
        this.appsList = updatedList;
        this.notifyDataSetChanged();
    }

    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                final ArrayList<EmailItem> results = new ArrayList<EmailItem>();
                if (appsList == null)
                    appsList = orig;
                if (constraint != null) {
                    if (orig != null && orig.size() > 0) {
                        for (final EmailItem g : orig) {
                            if ((g.getKey().toLowerCase()
                                    .contains(constraint.toString().toLowerCase())) ||
                                    (g.getValue().toLowerCase()
                                            .contains(constraint.toString().toLowerCase())))
                                results.add(g);
                        }
                    }
                    oReturn.values = results;
                }
                else
                {
                    oReturn.values = orig;
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,
                                          FilterResults results) {
                appsList = (List<EmailItem>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    @Override
    public int getCount() {
        return ((null != appsList) ? appsList.size() : 0);
    }

    @Override
    public EmailItem getItem(int position) {
        return ((null != appsList) ? appsList.get(position) : null);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (null == view) {
            LayoutInflater layoutInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.email_list_item, null);
        }


        EmailItem data = appsList.get(position);
        if (null != data) {

            TextView name = (TextView) view.findViewById(R.id.name_text);
            TextView email = (TextView) view.findViewById(R.id.email_text);

            name.setText(data.getKey());
            email.setText(data.getValue());
        }
        return view;
    }
}

