package com.idyllic_software.openup.utils;

/**
 * Created by javalnanda on 21/04/15.
 */
public class OpenUpConstants {

    //======= URL constants ===============//

//    public static final String BASE_URL = "http://628c5504.ngrok.com/api/v1/";
    public static final String BASE_URL = "http://openupapi.herokuapp.com/api/v1/";
    public static final String BASE_HTML_RESOURCE_URL = "http://openupapi.herokuapp.com/";

    public static final String COMMUNITY_STANDARDS_URL = BASE_HTML_RESOURCE_URL+ "/community_standards";
    public static final String PRIVACY_URL = BASE_HTML_RESOURCE_URL+ "/privacy";
    public static final String TERMS_OF_SERVICE_URL = BASE_HTML_RESOURCE_URL+ "/terms_of_services";
    public static final String ABOUT_US_URL = BASE_HTML_RESOURCE_URL+ "/about";

    public static final String REGISTER_USER_METHOD_NAME = "users";
    public static final String VERIFY_USER_METHOD_NAME = "users/verify";
    public static final String POST_STATUS_METHOD_NAME = "posts";
    public static final String COMMENT_METHOD_NAME = "posts/postid/reply";
    public static final String GET_FEED_METHOD_NAME = "posts";
    public static final String UPDATE_MOOD_METHOD_NAME = "users/update_mood";
    public static final String UPVOTE_METHOD_NAME = "posts/postid/upvote";
    public static final String DOWNVOTE_METHOD_NAME = "posts/postid/downvote";
    public static final String FLAG_METHOD_NAME = "posts/postid/flag";
    public static final String FOLLOW_METHOD_NAME = "posts/postid/follow";
    public static final String INVITE_METHOD_NAME = "users/invite";



    public static final String USER_TOKEN_PREFERENCES = "userToken";
    public static final String MOOD_UPDATE_DATE_PREFERENCES = "moodLastUpdatedOn";


    //=============Error Codes ============//
    public static final int ERROR_CODE_INVALID_USER = 1001;

}
