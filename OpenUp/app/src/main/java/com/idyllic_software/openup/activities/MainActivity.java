package com.idyllic_software.openup.activities;

import android.app.AlertDialog;
import android.support.v4.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.idyllic_software.openup.R;
import com.idyllic_software.openup.adapters.NavDrawerListAdapter;
import com.idyllic_software.openup.fragments.ContentWebviewFragment;
import com.idyllic_software.openup.fragments.HomeFragment;
import com.idyllic_software.openup.fragments.InviteFragment;
import com.idyllic_software.openup.view.CommentView;
import com.idyllic_software.openup.view.NavDrawerItem;
import com.idyllic_software.openup.services.OpenUpServiceManager;
import com.idyllic_software.openup.utils.OpenUpConstants;
import com.idyllic_software.openup.utils.SharedPreferenceManager;
import com.idyllic_software.openup.utils.Utility;
import com.loopj.android.http.JsonHttpResponseHandler;

import android.support.v4.app.Fragment;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity{

    private DrawerLayout mDrawerLayout;
    private RelativeLayout sideMenu;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;

    // slide menu items
    private String[] navMenuTitles;

    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;
    private Toolbar toolbar;
//    private ImageButton composeButton;
    private RelativeLayout composeLayout;
    private EditText composeEditText;
    private TextView textCounterText;
    private Button postButton;
    private Button inviteButton;
    private Button deleteAccountButton;

    private int currentMenu; // To keep track of selected fragment from the side menu.

    private HomeFragment homeFragment;

//    private ShakeEventManager sd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        OpenUpServiceManager.getInstance().initialize(MainActivity.this);

//        sd = new ShakeEventManager();
//        sd.setListener(this);
//        sd.init(this);

        mTitle = mDrawerTitle = getTitle();

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);


        sideMenu = (RelativeLayout) findViewById(R.id.side_menu);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        inviteButton = (Button) findViewById(R.id.button_invite);
        deleteAccountButton = (Button) findViewById(R.id.button_delete);

        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new InviteFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();


                // Don't set title when on Feed section.
                    setTitle("Invite People");

                mDrawerLayout.closeDrawer(sideMenu);
            }
        });

        deleteAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Delete Account")
                        .setMessage(getResources().getString(R.string.delete_confirmation_message))
                        .setCancelable(false)
                        .setPositiveButton("YES",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                //Remove user token
                                SharedPreferenceManager.savePreferences(MainActivity.this, OpenUpConstants.USER_TOKEN_PREFERENCES,"");
                                mDrawerLayout.closeDrawer(sideMenu);

                                Intent intent = new Intent(MainActivity.this,RegisterActivity.class);
                                startActivity(intent);

                                finish();
                            }
                        })
                        .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });
        navDrawerItems = new ArrayList<NavDrawerItem>();

        // adding nav drawer items to array
        for(int i=0;i<navMenuTitles.length;i++)
        {
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[i]));
        }

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setNavigationIcon(R.drawable.menu_icon);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_text_color));
        setSupportActionBar(toolbar);
        // enabling action bar app icon and behaving it as toggle button
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeButtonEnabled(true);
        composeLayout = (RelativeLayout) findViewById(R.id.compose_layout);

        composeEditText = (EditText) composeLayout.findViewById(R.id.post_edit_text);
        textCounterText = (TextView) composeLayout.findViewById(R.id.text_counter);
        postButton = (Button) composeLayout.findViewById(R.id.postButton);

//        composeButton = (ImageButton) toolbar.findViewById(R.id.composeButton);
//        composeButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               toggleComposeView();
//            }
//        });

        composeEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                textCounterText.setText(String.valueOf(140 - composeEditText.getText().toString().length()));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        /*postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (composeEditText.getText().toString().length()>0)
                {
                    OpenUpServiceManager.getInstance().postStatus(composeEditText.getText().toString(),new JsonHttpResponseHandler()
                    {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);

                            if (response.has("feed"))
                            {
                                try {
                                    OpenUpServiceManager.getInstance().getFeedArray().add(0,response.getJSONObject("feed"));
                                    homeFragment.updateFeedOnStatusPosted();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            toggleComposeView();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            super.onFailure(statusCode, headers, throwable, errorResponse);
                        }
                    });
                }
                else
                {
                    Utility.showAlertDialog(MainActivity.this,getResources().getString(R.string.enter_message));
                }
            }
        });*/
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                new Toolbar(MainActivity.this), //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ){
            public void onDrawerClosed(View view) {
//                if (mTitle.toString().equalsIgnoreCase(""))
//                    composeButton.setVisibility(View.VISIBLE);
//                else
//                    composeButton.setVisibility(View.GONE);

                toolbar.setTitle(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
//                composeButton.setVisibility(View.GONE);
//                if (composeLayout.getVisibility() == View.VISIBLE)
//                    toggleComposeView();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(0);
        }
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }

    private void slideToBottom(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,-view.getHeight(),0);
        animate.setDuration(200);
        animate.setFillAfter(true);
        animate.setInterpolator(new BounceInterpolator());
        view.startAnimation(animate);
        view.setVisibility(View.VISIBLE);
    }

    // To animate view slide out from bottom to top
    private void slideToTop(View view){
        TranslateAnimation animate = new TranslateAnimation(0,0,0,-view.getHeight());
        animate.setDuration(200);
        animate.setInterpolator(new BounceInterpolator());
        animate.setFillBefore(true);
        animate.setFillAfter(true);
        view.startAnimation(animate);
        view.setVisibility(View.GONE);
    }
    private void toggleComposeView()
    {
        if(composeLayout.getVisibility()==View.VISIBLE)
        {
//            composeLayout.setVisibility(View.GONE);
            /*composeLayout.animate()
                    .translationY(0)
                    .alpha(0.0f)
                    .setDuration(100)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            composeLayout.setVisibility(View.GONE);
                        }
                    });
            */

            /*Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    composeLayout.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            composeLayout.startAnimation(animation);
*/
            composeLayout.setVisibility(View.GONE);
            float dest = 0;

//            ObjectAnimator animation1 = ObjectAnimator.ofFloat(composeButton,
//                    "rotation", dest);
//            animation1.setDuration(200);
//            animation1.start();

            Utility.hideKeyboard(MainActivity.this);
//            slideToTop(composeLayout);
        }
        else
        {
            textCounterText.setText("140");
            composeEditText.setText("");
            composeEditText.requestFocus();
            composeLayout.setVisibility(View.VISIBLE);

            Utility.showKeyboard(MainActivity.this,composeEditText);
           /* Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            composeLayout.startAnimation(animation);
*/
            float dest = 45;

//            ObjectAnimator animation1 = ObjectAnimator.ofFloat(composeButton,
//                    "rotation", dest);
//            animation1.setDuration(200);
//            animation1.start();

//            slideToBottom(composeLayout);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        toolbar.setTitle(mTitle);
    }


    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /*@Override
    public void onShake() {
        if (currentMenu==0)
        {
            homeFragment.refreshEntireContent();
        }
    }*/

    @Override
    protected void onResume() {
        super.onResume();
//        sd.register();
    }


    @Override
    protected void onPause() {
        super.onPause();
//        sd.deregister();
    }
    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        String urlToLoad = null;
        switch (position) {
            case 0:
                fragment = new HomeFragment();
//                homeFragment = (HomeFragment) fragment;
                break;
            case 1:
                fragment = new ContentWebviewFragment();
                urlToLoad = OpenUpConstants.ABOUT_US_URL;
                break;
            case 2:
                fragment = new ContentWebviewFragment();
                urlToLoad = OpenUpConstants.TERMS_OF_SERVICE_URL;
                break;
            case 3:
                fragment = new ContentWebviewFragment();
                urlToLoad = OpenUpConstants.PRIVACY_URL;
                break;
            case 4:
                fragment = new ContentWebviewFragment();
                urlToLoad = OpenUpConstants.COMMUNITY_STANDARDS_URL;
                break;

            default:
                break;
        }

        if (fragment != null) {
            currentMenu = position;
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();

            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);

            // Don't set title when on Feed section.
            if(position==0)
            {
                setTitle("");

            }
            else
            {
                ((ContentWebviewFragment)fragment).setUrlString(urlToLoad);
                setTitle(navMenuTitles[position]);
            }

            mDrawerLayout.closeDrawer(sideMenu);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    public void showCommentView(final JSONObject feedData)
    {
        LayoutInflater layoutInflater =
                (LayoutInflater) getBaseContext().getSystemService(this.LAYOUT_INFLATER_SERVICE);
        final RelativeLayout commentView = (RelativeLayout) layoutInflater.inflate(R.layout.post_comment, null);

        Fragment f = getVisibleFragment();

        final RelativeLayout rootView = (RelativeLayout) f.getView().findViewById(R.id.root_view);
        rootView.addView(commentView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        RelativeLayout shareStatusLayout = (RelativeLayout) commentView.findViewById(R.id.share_status);

        final EditText commentEditText = (EditText) shareStatusLayout.findViewById(R.id.comment_edit_text);
        final TextView commentTextCounter = (TextView) shareStatusLayout.findViewById(R.id.comment_text_counter);
        ImageButton postCommentButton = (ImageButton) shareStatusLayout.findViewById(R.id.comment_button);

        commentView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                rootView.removeView(commentView);
                return false;
            }
        });

        postCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (commentEditText.getText().toString().length() > 0) {
                    try {
                        OpenUpServiceManager.getInstance().postComment(commentEditText.getText().toString(), Integer.parseInt(feedData.getString("post_id")), new JsonHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                super.onSuccess(statusCode, headers, response);

                                if (response.has("feed")) {

//                                    OpenUpServiceManager.getInstance().getFeedArray().add(0, response.getJSONObject("feed"));
//                                    listadapter.updateList(OpenUpServiceManager.getInstance().getFeedArray());
                                    Utility.hideKeyboard(MainActivity.this);
//                                    toggleCommentViewVisibility(parentFeedData);
//                                    composeEditText.setText("");
//                                    textCounterText.setText("140");
//                                    notifyAdminButton.setSelected(false);

                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                super.onFailure(statusCode, headers, throwable, errorResponse);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Utility.showAlertDialog(MainActivity.this, getResources().getString(R.string.enter_message));
                }
            }
        });

        commentEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                commentTextCounter.setText(String.valueOf(140 - commentEditText.getText().toString().length()));
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

    }

    public Fragment getVisibleFragment(){
        FragmentManager fragmentManager = MainActivity.this.getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            if(fragment != null && fragment.isVisible())
                return fragment;
        }
        return null;
    }
}
